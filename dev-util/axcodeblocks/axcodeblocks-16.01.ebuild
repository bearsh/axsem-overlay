# Copyright 1999-2016 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI="6"
WX_GTK_VER="2.8"

inherit eutils autotools wxwidgets

V_EXT="svn10778"

DESCRIPTION="An open source, cross platform, free C++ IDE for Axsem Microcontrollers"
HOMEPAGE="http://www.axsem.com"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
SRC_URI="https://bitbucket.org/bearsh/axsem/downloads/${P}${V_EXT}.tar.bz2"

IUSE="pch +sdcc +axdb +ax5043"

#S="${WORKDIR}/${P}.release"
S="${WORKDIR}/${P}${V_EXT}"

RDEPEND="app-arch/zip
	axdb? ( dev-embedded/microfoot )
	dev-embedded/microfoot-libmf
	dev-embedded/microfoot-libreent
	ax5043? ( dev-embedded/microfoot-libax5043 )
	sdcc? ( dev-embedded/sdcc[mcs51,sdbinutils,packihx] )
	x11-libs/wxGTK:${WX_GTK_VER}[X]
	app-text/hunspell
	dev-libs/boost:=
	dev-libs/libgamin
	sys-libs/zlib
"
DEPEND="${RDEPEND}
	app-text/dos2unix
	virtual/pkgconfig
"


src_prepare() {
	default
	# convert EOLs
	find . -type f -and -not -name "*.cpp" -and -not -name "*.h" -and -not -name "*.png" \
		-and -not -name "*.bmp" -and -not -name "*.c" -and -not -name "*.cxx" -and -not -name "*.ico" | \
	sed "s/.*/\"\\0\"/" | xargs dos2unix --keepdate &> /dev/null

	# remove execute bits from source files
	find src/plugins/contrib/regex_testbed -type f -exec chmod a-x {} ';'
	find src/plugins/contrib/IncrementalSearch -type f -exec chmod a-x {} ';'
	find src/plugins/compilergcc -type f -exec chmod a-x {} ';'

	# fix version string
	sed -i 's/-release//g' revision.m4 || die "sed failed"

	# fix sdcc tools bin name
	sed -i 's/sdcc-//' src/plugins/scriptedwizard/resources/mcs51/wizard.script || die "sed failed"
	sed -i 's/sdcc-//g' src/plugins/compilergcc/resources/compilers/options_sdcc.xml || die "sed failed"

	eautoreconf
	epatch "${FILESDIR}/${P}_include.patch"
}

src_configure() {
	#touch "${S}"/revision.m4 -r "${S}"/acinclude.m4

	setup-wxwidgets
	econf \
		--with-wx-config="${WX_CONFIG}" \
		--disable-debug \
		$(use_enable pch) \
		--disable-static \
		--enable-header-guard \
		--enable-log-hacker \
		--enable-modpoller \
		--enable-tidycmt \
		--with-contrib-plugins=all,-AutoVersioning,-byogames,-NassiShneiderman,-Valgrind,-wxcontrib,-wxsmith,-wxsmithcontrib,-wxsmithaui,-symtab,-MouseSap,-libfinder,-cbkoders
	# plugins:
	# AutoVersioning, BrowseTracker, byogames, Cccc, CppCheck, cbkoders, codesnippets,
	# codestat, copystrings, Cscope, DoxyBlocks, devpakupdater, dragscroll, EditorConfig, EditorTweaks, envvars,
	# FileManager, headerfixup, help, hexeditor, incsearch, keybinder, libfinder, MouseSap,
	# NassiShneiderman, ProjectOptionsManipulator, profiler, regex, ReopenEditor, exporter, smartindent, spellchecker,
	# symtab, ThreadSearch, ToolsPlus, Valgrind, wxcontrib, wxsmith, wxsmithcontrib, wxsmithaui
}

src_install() {
	default

	rm -f "${D}/usr/share/${PN}/compilers/compiler_sdcc.xml"
	rm -f "${D}/usr/share/${PN}/compilers/compiler_keilc51.xml"
}
