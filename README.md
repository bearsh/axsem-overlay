# Axsem overlay #

Contains IDE, tools and libraries for Axsem/OnSemi Microcontrollers

As this overlay is currently not listed in layman's index, add it with the following command as root to the local list of overlays:


```
#!bash

wget -O /etc/layman/overlays/axsem.xml https://bitbucket.org/bearsh/axsem/raw/HEAD/Documentation/overlay.xml
```
